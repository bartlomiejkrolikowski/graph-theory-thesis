Require Graphs.ModelMultiGraph.

Fact isIrreflexive_ofSucc :
  forall (n : nat) (g : ModelMultiGraph.t (S n)),
    ModelMultiGraph.isIrreflexive (S n) g ->
    ModelMultiGraph.isIrreflexive n (ModelMultiGraph.ofSucc g).
Proof.
  now unfold ModelMultiGraph.isIrreflexive, ModelMultiGraph.ofSucc.
Qed.

Fact isSymmetric_ofSucc :
  forall (n : nat) (g : ModelMultiGraph.t (S n)),
    ModelMultiGraph.isSymmetric (S n) g ->
    ModelMultiGraph.isSymmetric n (ModelMultiGraph.ofSucc g).
Proof.
  now unfold ModelMultiGraph.isSymmetric, ModelMultiGraph.ofSucc.
Qed.

Fact noMultiEdge_ofSucc :
  forall (n : nat) (g : ModelMultiGraph.t (S n)),
    ModelMultiGraph.noMultiEdge (S n) g ->
    ModelMultiGraph.noMultiEdge n (ModelMultiGraph.ofSucc g).
Proof.
  now unfold ModelMultiGraph.noMultiEdge, ModelMultiGraph.ofSucc.
Qed.

Fact isSimple_ofSucc :
  forall (n : nat) (g : ModelMultiGraph.t (S n)),
    ModelMultiGraph.isSimple (S n) g ->
    ModelMultiGraph.isSimple n (ModelMultiGraph.ofSucc g).
Proof.
  unfold ModelMultiGraph.isSimple. intros ? ? [HIrr [HSym HNoMul]].
  apply isIrreflexive_ofSucc in HIrr. apply isSymmetric_ofSucc in HSym.
  apply noMultiEdge_ofSucc in HNoMul. easy.
Qed.

Require Import Coq.Program.Equality.
Require Import FunctionalExtensionality.

Require Arith.

Require Import Graphs.MultiGraph.
Require Graphs.Aux.Fin.
Require Graphs.Aux.Proofs.Fin.
Require Graphs.Aux.Proofs.List.

(* edge *)

Fact edge_diag :
  forall (n : nat) (g : MultiGraph.t n) (u : Fin.t n),
    edge n g u u = 0.
Proof.
  induction g; intro.
  - now apply Fin.of_Fin_0.
  - dependent destruction u; now simpl.
Qed.

Fact edge_symm :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    edge n g u v = edge n g v u.
Proof.
  induction g; intros ? ?.
  - now apply Fin.of_Fin_0.
  - dependent destruction u; dependent destruction v; now simpl.
Qed.

Lemma edge_if_simple :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    isSimple g -> edge n g u v <= 1.
Proof.
  intros. induction H.
  - now apply Fin.of_Fin_0.
  - dependent destruction u; dependent destruction v; simpl; intuition.
Qed.

Lemma simple_if_edge_le_1 :
  forall (n : nat) (g : MultiGraph.t n),
    (forall u v : Fin.t n, edge n g u v <= 1) ->
    isSimple g.
Proof.
  induction g; simpl; intro Hedge.
  - apply simpEmpty.
  - apply simpCons.
    + intro. now specialize Hedge with Fin.F1 (Fin.FS u).
    + apply IHg. intros ? ?. now specialize Hedge with (Fin.FS u) (Fin.FS v).
Qed.

(* uniqueness *)

Fact edges_unique :
  forall (n : nat) (g h : MultiGraph.t n),
    (forall u v : Fin.t n, edge n g u v = edge n h u v) ->
    g = h.
Proof.
  induction g; dependent destruction h; intro HEqEdge.
  - reflexivity.
  - f_equal.
    + extensionality v. now specialize HEqEdge with Fin.F1 (Fin.FS v).
    + apply IHg. intros ? ?. now specialize HEqEdge with (Fin.FS u) (Fin.FS v).
Qed.

(* noEdge *)

Fact no_edge_1 :
  no_edge 1 = single.
Proof.
  simpl. unfold single. f_equal. now extensionality f.
Qed.

Fact NoEdge_in_no_edge :
  forall (n : nat) (u v : Fin.t n), edge n (no_edge n) u v = 0.
Proof.
  induction u; intro; dependent destruction v; now simpl.
Qed.

Fact no_edge_isSimple :
  forall n : nat, isSimple (no_edge n).
Proof.
  induction n; simpl.
  - apply simpEmpty.
  - apply simpCons.
    + intro. apply PeanoNat.Nat.le_0_l.
    + assumption.
Qed.

(* full *)

Fact full_1 :
  full 1 = single.
Proof.
  unfold full, single. f_equal. now extensionality f0.
Qed.

Fact disconnected_in_full :
  forall (n : nat) (u v : Fin.t n),
    edge n (full n) u v = 0 -> u = v.
Proof.
  induction u; intro; dependent destruction v; simpl; intro HNoEdge;
  [| | | f_equal; apply IHu ]; easy.
Qed.

Fact allEdges_in_full :
  forall (n : nat) (u v : Fin.t n),
    u <> v -> edge n (full n) u v = 1.
Proof.
  induction u; intros ? Hneq; dependent destruction v; simpl;
  [| | | apply IHu; intro; apply Hneq; f_equal]; easy.
Qed.

Fact full_isSimple :
  forall n : nat, isSimple (full n).
Proof.
  induction n; simpl.
  - apply simpEmpty.
  - apply simpCons.
    + intro. apply PeanoNat.Nat.le_refl.
    + assumption.
Qed.

(* add_edge *)

Lemma add_edge_diag_id :
  forall (n : nat) (g : MultiGraph.t n) (v : Fin.t n), add_edge n g v v = g.
Proof.
  induction g; intro.
  - reflexivity.
  - dependent destruction v; simpl.
    + reflexivity.
    + now f_equal.
Qed.

Lemma add_edge_valid :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    u <> v -> edge n (add_edge n g u v) u v = S (edge n g u v).
Proof.
  induction g; intros ? ? Hneq.
  - now apply Fin.of_Fin_0.
  - dependent destruction u; dependent destruction v; simpl.
    + contradiction.
    + apply Fin.S_point_point_value.
    + apply Fin.S_point_point_value.
    + apply IHg. intro Heq. apply Hneq. now f_equal.
Qed.

(* deg *)

(* correctness of deg - it is a sum of edges ending in u *)
Lemma deg_of_edge :
  forall (n : nat) (g : MultiGraph.t n) (u : Fin.t n),
    deg n g u = List.list_sum (List.map (edge n g u) (Fin.seq n)).
Proof.
  induction g; intro.
  - now apply Fin.of_Fin_0.
  - dependent destruction u; cbn.
    + now rewrite List.map_map.
    + rewrite <- PeanoNat.Nat.add_assoc. f_equal. fold (deg n g u).
      rewrite IHg. now rewrite List.map_map.
Qed.

Lemma deg_upper_bound :
  forall (n : nat) (g : MultiGraph.t n) (v : Fin.t n),
    isSimple g -> deg n g v < n.
Proof.
  intros ? ? ? Hsimp. induction Hsimp.
  - now apply Fin.of_Fin_0.
  - dependent destruction v; cbn.
    + apply Lt.le_lt_n_Sm.
      apply Le.le_trans with (length (Fin.seq n) * 1).
      * clear g Hsimp IHHsimp. rewrite <- List.map_length with (f := e).
        apply List.list_le_sum. induction n; simpl.
        -- contradiction.
        -- intros m HIn. destruct HIn as [HEq | HIn].
           ++ destruct HEq. apply H.
           ++ apply IHn with (e := fun v : Fin.t n => e (Fin.FS v)).
              ** intro. apply H.
              ** now rewrite <- List.map_map.
      * rewrite PeanoNat.Nat.mul_1_r. rewrite Fin.seq_length.
        apply Le.le_refl.
    + rewrite <- PeanoNat.Nat.add_assoc. rewrite <- PeanoNat.Nat.add_1_l.
      apply PeanoNat.Nat.add_le_lt_mono.
      * apply H.
      * fold (deg n g v). apply IHHsimp.
Qed.

Lemma full_deg :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    isSimple g ->
    deg n g u = Nat.pred n ->
    edge n g u v = 0 ->
    u = v.
Proof.
  intros ? ? ? ? Hsimple Hfull HNoEdge. induction Hsimple.
  - now apply Fin.of_Fin_0.
  - dependent destruction u; dependent destruction v.
    + reflexivity.
    + cbn in *. assert (e v = 1) as HEdge.
      * apply List.full_list_sum
          with (u := 1) (n := e v) (l := List.map e (Fin.seq n)).
        -- revert H. clear. induction n; simpl; intros ? ? HIn.
           ++ contradiction.
           ++ destruct HIn as [HEq | HIn].
              ** now destruct HEq.
              ** apply IHn with (e := fun v : Fin.t n => e (Fin.FS v)).
                 --- intro. apply H.
                 --- now rewrite <- List.map_map.
        -- rewrite List.map_length. rewrite Fin.seq_length.
           now rewrite PeanoNat.Nat.mul_1_r.
        -- apply List.in_map. apply Fin.seq_complete.
      * rewrite HNoEdge in HEdge. discriminate.
    + revert Hsimple Hfull. cbn. simpl in HNoEdge. rewrite HNoEdge. simpl.
      fold (deg n g u). clear.
      specialize deg_upper_bound with (n := n) (g := g) (v := u).
      intros HUpBound ? ?. rewrite Hfull in HUpBound.
      apply HUpBound in Hsimple as Hlt. now apply PeanoNat.Nat.lt_irrefl in Hlt.
    + cbn in *. apply f_equal. apply IHHsimple.
      * specialize H with u.
        destruct (e u) as [| m]; simpl in *; fold (deg n g u) in *.
        -- specialize deg_upper_bound with (n := n) (g := g) (v := u)
             as deg_upper_bound.
           rewrite Hfull in deg_upper_bound.
           apply deg_upper_bound in Hsimple as Hlt.
           now apply PeanoNat.Nat.lt_irrefl in Hlt.
        -- apply le_S_n in H. apply PeanoNat.Nat.le_0_r in H as Hm.
           rewrite Hm in Hfull. simpl in Hfull.
           assert (n = S (Nat.pred n)) as Hpred by now rewrite <- Hfull.
           apply eq_trans with (z := S (Nat.pred n)) in Hfull.
           ++ now injection Hfull.
           ++ assumption.
      * assumption.
Qed.

Lemma zero_deg :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    deg n g u = 0 -> edge n g u v = 0.
Proof.
  induction g; intros ? ? Hdeg.
  - now apply Fin.of_Fin_0.
  - dependent destruction u.
    + dependent destruction v; simpl.
      * reflexivity.
      * now apply Fin.zero_sum.
    + dependent destruction v; cbn in *;
      rewrite <- PeanoNat.Nat.add_assoc in Hdeg;
      apply PeanoNat.Nat.eq_add_0 in Hdeg as [He Hdeg]; intuition.
Qed.

Fact zero_and_full_deg_vertex :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    isSimple g ->
    deg n g u = Nat.pred n ->
    deg n g v = 0 ->
    u = v.
Proof.
  intros ? ? ? ? Hsimple Hfull Hzero. apply zero_deg with (v := u) in Hzero.
  rewrite edge_symm in Hzero.
  now apply full_deg with (v := v) in Hfull.
Qed.

Fact zero_and_full_deg_size :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    isSimple g ->
    deg n g u = Nat.pred n ->
    deg n g v = 0 ->
    n = 0 \/ n = 1.
Proof.
  intros ? ? ? ? Hsimple Hfull Hzero. apply PeanoNat.Nat.eq_pred_0.
  rewrite <- Hfull.
  now rewrite zero_and_full_deg_vertex with (g := g) (u := u) (v := v).
Qed.

(* zadanie 1. z listy z teorii grafow *)
(* deg is not an injection *)
Theorem deg_not_inj :
  forall (n : nat) (g : MultiGraph.t n),
    isSimple g ->
    2 <= n ->
    exists u v : Fin.t n, u <> v /\ deg n g u = deg n g v.
Proof.
  intros ? ? Hsimp Hn. apply Fin.fin_n_not_inj. intro Hinj.
  pose proof (H0 := Fin.fin_n_inj_lt_n n 0 (deg n g)).
  pose proof (HnPred :=
    Fin.fin_n_inj_lt_n n (Nat.pred n) (deg n g)).
  pose proof (HUpBound := deg_upper_bound n g).
  destruct H0, HnPred; intuition.
  apply zero_and_full_deg_size with (u := x0) (v := x) in H0; trivial.
  destruct H0 as [H0 | H0]; rewrite H0 in Hn; inversion Hn as [m | m Hle Heq].
  inversion Hle.
Qed.

Lemma sum_edges_is_valid_for_no_edges :
  forall n : nat, sum_edges n (no_edge n) = 0.
Proof.
  induction n; simpl.
  - reflexivity.
  - apply PeanoNat.Nat.eq_add_0. split.
    + rewrite List.map_const. apply List.sum_of_zeros.
    + assumption.
Qed.

Lemma sum_edges_is_valid_for_succ_edges :
  forall (n : nat) (g : MultiGraph.t n) (u v : Fin.t n),
    u <> v -> sum_edges n (add_edge n g u v) = S (sum_edges n g).
Proof.
  induction g; intros ? ? Hneq.
  - now apply Fin.of_Fin_0.
  - dependent destruction u; dependent destruction v; simpl in *.
    + contradiction.
    + fold (Fin.sum (Fin.S_point v edges)).
      now rewrite Fin.sum_S_point.
    + fold (Fin.sum (Fin.S_point u edges)).
      now rewrite Fin.sum_S_point.
    + rewrite IHg.
      * intuition.
      * intro Heq. apply Hneq. now f_equal.
Qed.

Lemma sum_deg_backward_is_sum_edges :
  forall (n : nat) (g : MultiGraph.t n),
    List.list_sum (List.map (deg_backward n g) (Fin.seq n)) = sum_edges n g.
Proof.
  induction g; simpl.
  - reflexivity.
  - f_equal. rewrite List.map_map. apply IHg.
Qed.

Lemma sum_deg_forward_is_sum_edges :
  forall (n : nat) (g : MultiGraph.t n),
    List.list_sum (List.map (deg_forward n g) (Fin.seq n)) = sum_edges n g.
Proof.
  induction g; simpl.
  - reflexivity.
  - rewrite List.map_map.
    rewrite List.map_ext
      with (g := Fin.add edges (deg_forward n g)).
    + fold (Fin.sum (Fin.add edges (deg_forward n g))).
      rewrite Fin.sum_add. now f_equal.
    + trivial.
Qed.

(* Euler's sum of degrees theorem *)
Theorem euler_sum_deg :
  forall (n : nat) (g : MultiGraph.t n),
    List.list_sum (List.map (deg n g) (Fin.seq n)) = 2 * sum_edges n g.
Proof.
  intros ? ?. unfold deg.
  rewrite List.map_ext
    with (g := Fin.add (deg_forward n g) (deg_backward n g)).
  - match goal with
    | |- context[List.list_sum (List.map ?x _)] => fold (Fin.sum x)
    end.
    rewrite Fin.sum_add. unfold Fin.sum.
    rewrite sum_deg_forward_is_sum_edges, sum_deg_backward_is_sum_edges.
    simpl. intuition.
  - trivial.
Qed.

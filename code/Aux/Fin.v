Require Nat.
Require Fin.
Require List.
Import List.ListNotations.

Definition empty_of_Fin_0 (f : Fin.t 0) : Empty_set:=
  let T (n : nat) : Type :=
    match n with
    | 0 => Empty_set
    | _ => unit
    end
  in match f in Fin.t n return T n with
     | Fin.F1 => tt
     | Fin.FS _ => tt
     end.

Definition of_Fin_0 {A : Type} (f : Fin.t 0) : A :=
  match empty_of_Fin_0 f return A with end.

Fixpoint toNat {n : nat} (f : Fin.t n) : nat :=
  match f with
  | Fin.F1 => 0
  | Fin.FS f' => S (toNat f')
  end.

Fixpoint fin_ofNat (n k : nat) : Fin.t (S n) :=
  match n as m, k return Fin.t (S m) with
  | 0, _ | _, 0 => Fin.F1
  | S n', S k' => Fin.FS (fin_ofNat n' k')
  end.

Fixpoint ofNat (n : nat) : option (Fin.t (S n)) :=
  match n as k return option (Fin.t (S k)) with
  | 0 => Some Fin.F1
  | S n' =>
    match ofNat n' with
    | Some f => Some (Fin.FS f)
    | None => None
    end
  end.

Fixpoint toNat1n {n : nat} (f : Fin.t n) : nat :=
  match f with
  | Fin.F1 => 1
  | Fin.FS f' => S (toNat1n f')
  end.

Definition ofNat1n (n : nat) : option (Fin.t n) :=
  match n as k return option (Fin.t k) with
  | 0 => None
  | S n' => ofNat n'
  end.

Fixpoint toNatRev {n : nat} (f : Fin.t n) : nat :=
  match f with
  | @Fin.F1 n' => n'
  | Fin.FS f' => toNatRev f'
  end.

Fixpoint toNatRev1n {n : nat} (f : Fin.t n) : nat :=
  match f with
  | Fin.F1 => n
  | Fin.FS f' => toNatRev1n f'
  end.

Fixpoint seq (n : nat) : list (Fin.t n) :=
  match n with
  | 0 => []
  | S n' => Fin.F1 :: List.map Fin.FS (seq n')
  end.

Definition sum {n : nat} (F : Fin.t n -> nat) : nat :=
  List.list_sum (List.map F (seq n)).

Definition S_point {n : nat} (p : Fin.t n) (F : Fin.t n -> nat)
  : Fin.t n -> nat :=
  fun f : Fin.t n => if Fin.eqb f p then S (F f) else F f.

Definition point {n : nat} (p : Fin.t n) : Fin.t n -> nat :=
  fun f : Fin.t n => if Fin.eqb f p then 1 else 0.

Definition add {n : nat} (F G : Fin.t n -> nat) : Fin.t n -> nat :=
  fun f : Fin.t n => F f + G f.

Fixpoint fin_list_pred {n : nat} (l : list (Fin.t n)) :
  list (Fin.t (Nat.pred n)) :=
  match l with
  | nil => nil
  | cons f l' =>
    match f in Fin.t k
      return list (Fin.t (Nat.pred k)) -> list (Fin.t (Nat.pred k))
    with
    | @Fin.F1 n' => fun l : list (Fin.t n') => l
    | @Fin.FS n' f' => fun l : list (Fin.t n') => cons f' l
    end (fin_list_pred l')
  end.

Fixpoint skip {n : nat} (s : Fin.t n) :
  Fin.t n -> option (Fin.t (Nat.pred n)) :=
  match s in Fin.t k return Fin.t k -> option (Fin.t (Nat.pred k)) with
  | Fin.F1 => fun f =>
    match f in Fin.t k return option (Fin.t (Nat.pred k)) with
    | Fin.F1 => None
    | Fin.FS f' => Some f'
    end
  | Fin.FS s' => fun f =>
    match f in Fin.t k
      return (Fin.t (Nat.pred k) -> option (Fin.t (Nat.pred (Nat.pred k)))) ->
             Fin.t (Nat.pred k) -> option (Fin.t (Nat.pred k))
    with
    | Fin.F1 => fun _ => Some
    | @Fin.FS n' f' =>
      match f' with
      | Fin.F1 as f'' | Fin.FS _ as f'' =>
        fun (F : Fin.t _ -> option (Fin.t (Nat.pred _))) _ =>
          option_map Fin.FS (F f'')
      end
    end (skip s')
      (match s' in Fin.t k return Fin.t k with
         | Fin.F1 | Fin.FS _ => Fin.F1
       end)
  end.

Definition skip_fun {n m : nat} (def : Fin.t m) (F : Fin.t (S n) -> Fin.t (S m))
  (f : Fin.t n) : Fin.t m :=
  match skip (F Fin.F1) (F (Fin.FS f)) with
  | Some f' => f'
  | None => def
  end.

Definition skip_always {n : nat} (s : Fin.t (S (S n))) (f : Fin.t (S (S n))) :
  Fin.t (S n) :=
  match skip s f with
  | Some f' => f'
  | None => Fin.F1
  end.

Definition is_F1_nat {n : nat} (f : Fin.t n) : nat :=
  match f with
  | Fin.F1 => 1
  | Fin.FS _ => 0
  end.

Definition count_F1 {n : nat} (l : list (Fin.t n)) : nat :=
  List.list_sum (List.map is_F1_nat l).

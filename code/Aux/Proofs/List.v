Require Nat.
Require Arith.
Require List.
Import List.ListNotations.

Lemma list_le_sum :
  forall (u : nat) (l : list nat),
    (forall n : nat, List.In n l -> n <= u) ->
    List.list_sum l <= length l * u.
Proof.
  induction l; simpl.
  - trivial.
  - intro. apply PeanoNat.Nat.add_le_mono.
    + apply H. now left.
    + apply IHl. intros ? H'. apply H. now right.
Qed.

Lemma full_list_sum :
  forall (u n : nat) (l : list nat),
    (forall m : nat, List.In m l -> m <= u) ->
    List.list_sum l = length l * u ->
    List.In n l -> n = u.
Proof.
  induction l; simpl; intros HUpBound HLSum HIn.
  - contradiction.
  - destruct HIn as [HEq | HIn].
    + destruct HEq. assert (u <= a) as H.
      * apply PeanoNat.Nat.le_le_add_le with (List.list_sum l) (length l * u).
        -- apply list_le_sum. intuition.
        -- now rewrite HLSum.
      * specialize HUpBound with a; intuition.
    + apply IHl.
      * intuition.
      * apply PeanoNat.Nat.le_antisymm.
        -- apply list_le_sum. intuition.
        -- apply PeanoNat.Nat.le_le_add_le with a u.
          ++ apply HUpBound. now left.
          ++ rewrite PeanoNat.Nat.add_comm with _ u,
                     PeanoNat.Nat.add_comm with _ a.
             now rewrite HLSum.
      * assumption.
Qed.

Fact list_ge_sum :
  forall (l : nat) (nats : list nat),
    (forall n : nat, List.In n nats -> l <= n) ->
    length nats * l <= List.list_sum nats.
Proof.
  induction nats; simpl; intro HLowBound.
  - apply Le.le_refl.
  - apply PeanoNat.Nat.add_le_mono; intuition.
Qed.

Lemma low_list_sum :
  forall (l n : nat) (nats : list nat),
    (forall m : nat, List.In m nats -> l <= m) ->
    List.list_sum nats = length nats * l ->
    List.In n nats ->
    n = l.
Proof.
  induction nats; simpl; intros HLowBound Hsum HIn.
  - contradiction.
  - destruct HIn as [HEq | HIn].
    + apply Le.le_antisym.
      * apply PeanoNat.Nat.le_le_add_le
          with (length nats * l) (List.list_sum nats).
        -- apply list_ge_sum. intuition.
        -- destruct HEq. now apply PeanoNat.Nat.eq_le_incl.
      * apply HLowBound. now left.
    + apply IHnats.
      * intuition.
      * apply Le.le_antisym.
        -- apply PeanoNat.Nat.le_le_add_le with l a.
           ++ intuition.
           ++ rewrite PeanoNat.Nat.add_comm with _ a.
              rewrite PeanoNat.Nat.add_comm with _ l.
              now apply PeanoNat.Nat.eq_le_incl.
        -- apply list_ge_sum. intuition.
      * assumption.
Qed.

Fact zero_list_sum :
  forall (n : nat) (l : list nat),
    List.list_sum l = 0 ->
    List.In n l ->
    n = 0.
Proof.
  intros. apply low_list_sum with (nats := l).
  - intuition.
  - now rewrite PeanoNat.Nat.mul_0_r.
  - assumption.
Qed.

Fact zero_list_sum_eq_zeros :
  forall l : list nat,
    List.list_sum l = 0 -> l = List.repeat 0 (length l).
Proof.
  induction l; simpl; intro Hsum.
  - reflexivity.
  - apply PeanoNat.Nat.eq_add_0 in Hsum. f_equal; intuition.
Qed.

Fact sum_of_zeros :
  forall n : nat,
    List.list_sum (List.repeat 0 n) = 0.
Proof.
  induction n; simpl.
  - reflexivity.
  - assumption.
Qed.

Fact sum_of_eq_zeros :
  forall l : list nat,
    l = List.repeat 0 (length l) ->
    List.list_sum l = 0.
Proof.
  induction l; simpl; intro Hl.
  - reflexivity.
  - injection Hl as Hhd Htl. now rewrite Hhd, IHl.
Qed.

Fact map_const :
  forall (A B : Type) (b : B) (l : list A),
    List.map (fun _ => b) l = List.repeat b (length l).
Proof.
  induction l; simpl.
  - reflexivity.
  - now f_equal.
Qed.

Fact In_map_inj :
  forall (A B : Type) (f : A -> B) (l : list A) (a : A),
    (forall a a' : A, f a = f a' -> a = a') ->
    List.In (f a) (List.map f l) ->
    List.In a l.
Proof.
  induction l; simpl; intros ? Hinj HIn.
  - assumption.
  - destruct HIn as [HEq | HIn].
    + left. now apply Hinj.
    + right. now apply IHl.
Qed.

Fact NoDup_inj :
  forall (A B : Type) (f : A -> B) (l : list A),
    (forall a a' : A, f a = f a' -> a = a') ->
    List.NoDup l ->
    List.NoDup (List.map f l).
Proof.
  intros ? ? ? ? HInj HNoDup. induction HNoDup; simpl.
  - apply List.NoDup_nil.
  - apply List.NoDup_cons.
    + intro HIn. apply H. apply In_map_inj with (f := f); assumption.
    + assumption.
Qed.

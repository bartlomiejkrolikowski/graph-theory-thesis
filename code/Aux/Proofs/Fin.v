Require Arith.
Require Import Coq.Program.Equality.
Require Graphs.Aux.Proofs.List.

Require Import Graphs.Aux.Fin.

Fact fin_F1_neq_FS :
  forall (n : nat) (f : Fin.t n), Fin.F1 <> Fin.FS f.
Proof.
  intros ? ? HEq. inversion HEq.
Qed.

Fact fin_F1_not_In_FS_list :
  forall (n : nat) (l : list (Fin.t n)),
    ~ List.In Fin.F1 (List.map Fin.FS l).
Proof.
  induction l; cbn; intro HIn.
  - assumption.
  - destruct HIn as [HEq | HIn].
    + now apply fin_F1_neq_FS with (f := a).
    + now apply IHl.
Qed.

Fact all_fin_1 :
  forall f : Fin.t 1, f = Fin.F1.
Proof.
  now dependent destruction f.
Qed.

Fact fin_ofNat_inj :
  forall n k k' : nat,
    k <= n ->
    k' <= n ->
    fin_ofNat n k = fin_ofNat n k' ->
    k = k'.
Proof.
  induction n; destruct k, k'; intros kLe k'Le Heq; try easy. f_equal.
  apply IHn; intuition. now apply Fin.FS_inj.
Qed.

Fact toNat_fin_ofNat :
  forall (n : nat) (f : Fin.t (S n)), fin_ofNat n (toNat f) = f.
Proof.
  dependent induction f; destruct n; try easy. simpl. f_equal. now apply IHf.
Qed.

Fact fin_ofNat_toNat :
  forall (n k : nat),
    k <= n ->
    toNat (fin_ofNat n k) = k.
Proof.
  induction n; destruct k; intro Hle; try easy. simpl. f_equal. apply IHn.
  intuition.
Qed.

(* seq *)

Fact seq_length :
  forall n : nat, length (Fin.seq n) = n.
Proof.
  induction n; simpl.
  - reflexivity.
  - f_equal. rewrite List.map_length. exact IHn.
Qed.

Fact seq_complete :
  forall (n : nat) (f : Fin.t n), List.In f (Fin.seq n).
Proof.
  induction f; simpl.
  - now left.
  - right. now apply List.in_map.
Qed.

Fact seq_fin_NoDup :
  forall n : nat, List.NoDup (Fin.seq n).
Proof.
  induction n; simpl.
  - apply List.NoDup_nil.
  - apply List.NoDup_cons.
    + apply fin_F1_not_In_FS_list.
    + apply List.NoDup_inj.
      * apply Fin.FS_inj.
      * assumption.
Qed.

(* point *)

Fact FS_bool_inj :
  forall (n : nat) (f f' : Fin.t n),
    Fin.eqb (Fin.FS f) (Fin.FS f') = Fin.eqb f f'.
Proof.
  trivial.
Qed.

Fact sum_point :
  forall (n : nat) (p : Fin.t n), sum (point p) = 1.
Proof.
  unfold sum, point. induction p; simpl.
  - rewrite PeanoNat.Nat.eqb_refl. simpl. f_equal.
    rewrite List.map_map, List.map_ext with (g := fun _ => 0).
    + rewrite List.map_const. apply List.sum_of_zeros.
    + intro. destruct Fin.eqb eqn:Heqb.
      * apply Fin.eqb_eq in Heqb as Heq. now pose proof fin_F1_neq_FS as Hneq.
      * reflexivity.
  - rewrite List.map_map, List.map_ext with (g := point p); easy.
Qed.

(* add *)

Fact sum_add :
  forall (n : nat) (F G : Fin.t n -> nat), sum (add F G) = sum F + sum G.
Proof.
  unfold sum, add. induction n; simpl; intros ? ?.
  - reflexivity.
  - rewrite PeanoNat.Nat.add_assoc.
    match goal with
    | |- context[_ = ?x + ?y + _] => rewrite PeanoNat.Nat.add_comm with x y
    end.
    rewrite PeanoNat.Nat.add_assoc.
    match goal with
    | |- context[_ = ?x + ?y + _ + _] => rewrite PeanoNat.Nat.add_comm with x y
    end.
    match goal with
    | |- context[_ = ?x + ?y + ?z] =>
      rewrite <- PeanoNat.Nat.add_assoc with x y z
    end.
    f_equal. rewrite List.map_map, List.map_map, List.map_map. apply IHn.
Qed.

(* S_point *)

Fact S_point_is_valid :
  forall (n : nat) (F : Fin.t n -> nat) (p f : Fin.t n),
    S_point p F f = add (point p) F f.
Proof.
  intros. unfold S_point, add, point. now destruct Fin.eqb.
Qed.

Fact sum_S_point :
  forall (n : nat) (F : Fin.t n -> nat) (p : Fin.t n),
    sum (S_point p F) = S (sum F).
Proof.
  intros ? ? ?. unfold sum. rewrite List.map_ext with (g := add (point p) F).
  - fold (sum (add (point p) F)). fold (sum F). rewrite sum_add.
    now rewrite sum_point.
  - apply S_point_is_valid.
Qed.

Fact S_point_point_value :
  forall (n : nat) (F : Fin.t n -> nat) (p : Fin.t n),
    S_point p F p = S (F p).
Proof.
  intros ? ? ?. unfold Fin.S_point. pose (Heq := eq_refl p).
  apply Fin.eqb_eq in Heq. now rewrite Heq.
Qed.

Fact S_point_out_value :
  forall (n : nat) (F : Fin.t n -> nat) (p f : Fin.t n),
    p <> f ->
    S_point p F f = F f.
Proof.
  intros ? ? ? ? Hneq. unfold Fin.S_point. destruct Fin.eqb eqn:Heqb.
  - apply Fin.eqb_eq in Heqb. destruct Heqb. contradiction.
  - reflexivity.
Qed.

(* sum *)

Fact zero_sum :
  forall (n : nat) (F : Fin.t n -> nat) (f : Fin.t n),
    sum F = 0 -> F f = 0.
Proof.
  intros ? ? ?. unfold sum.
  match goal with
  | |- context[List.list_sum ?x] =>
    intro Hsum; apply List.zero_list_sum with (l := x)
  end.
  - assumption.
  - apply List.in_map. apply seq_complete.
Qed.

(* other *)

Fact fin_list_pred_all_FS :
  forall (n : nat) (l : list (Fin.t (S n))),
    (forall f : Fin.t (S n),
       exists f' : Fin.t n, List.In f l -> f = Fin.FS f') ->
    l = List.map Fin.FS (Fin.fin_list_pred l).
Proof.
  induction l; simpl; intro HFS.
  - reflexivity.
  - destruct HFS with a as [a' Ha]. rewrite Ha.
    + simpl. f_equal. apply IHl. intro. destruct HFS with f as [f' Hf]. eauto.
    + now left.
Qed.

Fact count_F1_le_length :
  forall (n : nat) (l : list (Fin.t n)), count_F1 l <= length l.
Proof.
  induction l; simpl.
  - apply Le.le_refl.
  - destruct a; cbn.
    + intuition.
    + now apply le_S.
Qed.

Lemma sub_succ_r :
  forall n m : nat, m < n -> S (n - (S m)) = n - m.
Proof.
  intros ? ? Hlt. destruct n; simpl.
  - now apply PeanoNat.Nat.nlt_0_r in Hlt.
  - rewrite <-PeanoNat.Nat.sub_succ_l; intuition.
Qed.

Fact fin_list_pred_length :
  forall (n : nat) (l : list (Fin.t n)),
    length (fin_list_pred l) = length l - count_F1 l.
Proof.
  induction l; simpl.
  - reflexivity.
  - destruct a; cbn.
    + assumption.
    + pose proof (count_F1_le_length (S n) l) as count_F1_le_length.
      unfold count_F1, List.list_sum in *. destruct List.fold_right.
      * rewrite PeanoNat.Nat.sub_0_r in IHl. now f_equal.
      * simpl in *. rewrite IHl. now apply sub_succ_r.
Qed.

Fact list_fin_0 :
  forall l : list (Fin.t 0), l = nil.
Proof.
  destruct l.
  - reflexivity.
  - now apply Fin.of_Fin_0.
Qed.

Lemma fin_list_pred_ind :
  forall P : forall n : nat, list (Fin.t n) -> Prop,
    (forall n : nat, P n nil) ->
    (forall (n : nat) (l : list (Fin.t n)),
       P (Nat.pred n) (fin_list_pred l) -> P n l) ->
    forall (n : nat) (l : list (Fin.t n)), P n l.
Proof.
  intros ? Hnil Hind. induction n; intro.
  - now rewrite list_fin_0.
  - now apply Hind.
Qed.

Fact fin_list_pred_FS_In_ind :
  forall (n : nat) (a : Fin.t n) (l : list (Fin.t n)) (f : Fin.t (Nat.pred n)),
    match n as k
      return Fin.t k -> list (Fin.t k) -> Fin.t (Nat.pred k) -> Prop with
    | 0 => fun _ _ _ => True
    | S n' => fun (a : Fin.t (S n')) (l : list (Fin.t (S n'))) (f : Fin.t n') =>
      (forall f : Fin.t n', List.In (Fin.FS f) l -> List.In f (fin_list_pred l)) ->
      List.In (Fin.FS f) (cons a l) -> List.In f (fin_list_pred (cons a l))
    end a l f.
Proof.
  destruct a; simpl; intros ? ? IHl HIn; destruct HIn as [HEq | HIn].
  - now apply IHl.
  - now apply IHl.
  - left. now apply Fin.FS_inj.
  - right. now apply IHl.
Qed.

Fact fin_list_pred_FS_In :
  forall (n : nat) (l : list (Fin.t (S n))) (f : Fin.t n),
    List.In (Fin.FS f) l -> List.In f (fin_list_pred l).
Proof.
  induction l; simpl; intros ? HIn.
  - assumption.
  - specialize fin_list_pred_FS_In_ind with (a := a) (l := l) (f := f). simpl.
    intuition.
Qed.

Fact all_fin_fin_list_pred :
  forall (n : nat) (l : list (Fin.t n)),
    (forall f : Fin.t n, List.In f l) ->
    forall f : Fin.t (Nat.pred n), List.In f (fin_list_pred l).
Proof.
  destruct n; simpl; intros ? HIn ?.
  - now apply Fin.of_Fin_0.
  - now apply fin_list_pred_FS_In.
Qed.

Fact positive_count_F1 :
  forall (n : nat) (l : list (Fin.t (S n))),
    List.In Fin.F1 l -> 0 < count_F1 l.
Proof.
  intros ? ? HIn. apply List.in_split in HIn as [l' [l'' HIn]].
  rewrite HIn. unfold count_F1. rewrite List.map_app, List.list_sum_app. simpl.
  rewrite PeanoNat.Nat.add_succ_r. apply PeanoNat.Nat.lt_0_succ.
Qed.

Fact fin_list_pred_In_F1_length :
  forall (n : nat) (l : list (Fin.t (S n))),
    List.In Fin.F1 l -> length (fin_list_pred l) < length l.
Proof.
  intros ? ? HIn. rewrite fin_list_pred_length. apply PeanoNat.Nat.sub_lt.
  - apply count_F1_le_length.
  - now apply positive_count_F1.
Qed.

Fact all_fin_list :
  forall (n : nat) (l : list (Fin.t n)),
    (forall f : Fin.t n, List.In f l) ->
    n <= length l.
Proof.
  intros ? ?.
  apply fin_list_pred_ind with (n := n) (l := l); clear; simpl.
  - destruct n; intro HIn.
    + apply Le.le_refl.
    + destruct HIn. exact Fin.F1.
  - intros ? ? Hflp HIn. destruct n eqn:Hn.
    + apply PeanoNat.Nat.le_0_l.
    + simpl in Hflp. apply Le.le_trans with (S (length (fin_list_pred l))).
      * apply Le.le_n_S. apply Hflp. intro.
        now apply all_fin_fin_list_pred with (n := S n0).
      * now apply fin_list_pred_In_F1_length.
Qed.

(* Some is injection *)
Fact Some_inj :
  forall (A : Type) (a a' : A), Some a = Some a' -> a = a'.
Proof.
  intros ? ? ? HEq. now injection HEq.
Qed.

(* (option_map Fin.FS) is injection *)
Fact option_map_FS_inj :
  forall (n : nat) (of of' : option (Fin.t n)),
    option_map Fin.FS of = option_map Fin.FS of' -> of = of'.
Proof.
  destruct of, of'; simpl; intro HEq.
  - f_equal. apply Fin.FS_inj. now apply Some_inj.
  - discriminate.
  - discriminate.
  - easy.
Qed.

(* skip is injection *)
Fact skip_inj :
  forall (n : nat) (s f f' : Fin.t n), skip s f = skip s f' -> f = f'.
Proof.
  induction s.
  - dependent destruction f; dependent destruction f'; simpl; intro Hskip.
    + easy.
    + discriminate.
    + discriminate.
    + f_equal. now injection Hskip.
  - dependent destruction f; dependent destruction f'; simpl; intro Hskip.
    + easy.
    + dependent destruction s; dependent destruction f'; destruct skip;
      simpl in Hskip; discriminate.
    + dependent destruction s; dependent destruction f; destruct skip;
      simpl in Hskip; discriminate.
    + f_equal. apply IHs.
      dependent destruction f; dependent destruction f';
      simpl in Hskip; now apply option_map_FS_inj.
Qed.

(* skip is None only for equal arguments *)
Fact skip_none :
  forall (n : nat) (s f : Fin.t n), skip s f = None -> s = f.
Proof.
  induction s; dependent destruction f; simpl; intro Hskip.
  - easy.
  - easy.
  - easy.
  - f_equal. apply IHs. destruct f; now destruct skip.
Qed.

(* skip_fun is injection *)
Fact skip_fun_inj :
  forall (n m : nat) (def : Fin.t m) (F : Fin.t (S n) -> Fin.t (S m))
    (f f' : Fin.t n),
    (forall sf sf' : Fin.t (S n), F sf = F sf' -> sf = sf') ->
    skip_fun def F f = skip_fun def F f' -> f = f'.
Proof.
  intros ? ? ? ? ? ? Hinj. unfold skip_fun.
  destruct (skip _ (F (Fin.FS f))) eqn:Hskip,
           (skip _ (F (Fin.FS f'))) eqn:Hskip'; intro HEq.
  - rewrite HEq in Hskip. rewrite <- Hskip' in Hskip. apply skip_inj in Hskip.
    apply Fin.FS_inj. now apply Hinj.
  - apply skip_none in Hskip'. apply Hinj in Hskip'. discriminate.
  - apply skip_none in Hskip. apply Hinj in Hskip. discriminate.
  - apply skip_none in Hskip. apply Hinj in Hskip. discriminate.
Qed.

Fact Fin_FS_if_not_F1 :
  forall (n : nat) (f : Fin.t (S n)),
    f <> Fin.F1 -> exists f' : Fin.t n, f = Fin.FS f'.
Proof.
  dependent destruction f.
  - easy.
  - eauto.
Qed.

Fact Fin_F1_or_FS :
  forall (n : nat) (f : Fin.t (S n)),
    f = Fin.F1 \/ exists f' : Fin.t n, f = Fin.FS f'.
Proof.
  dependent destruction f.
  - now left.
  - right. eauto.
Qed.

(* Dirichlet *)
Theorem drawer_principle :
  forall (n m : nat) (F : Fin.t n -> Fin.t m),
    (forall f f' : Fin.t n, F f = F f' -> f = f') ->
    n <= m.
Proof.
  induction n; intros ? ? Hinj.
  - apply PeanoNat.Nat.le_0_l.
  - destruct m.
    + apply of_Fin_0. apply F. exact Fin.F1.
    + apply le_n_S. pose proof (HFF1 := Fin_F1_or_FS m (F Fin.F1)).
      destruct HFF1 as [HFF1 | HFF1].
      * destruct n as [| n'].
        -- apply PeanoNat.Nat.le_0_l.
        -- pose proof (HFFS1eqFS := Fin_FS_if_not_F1 m (F (Fin.FS Fin.F1))).
           destruct HFFS1eqFS.
           ++ rewrite <- HFF1. intro HEq. now apply Hinj in HEq.
           ++ apply IHn with (F := fun f => skip_fun x F f). intros ? ?.
              now apply skip_fun_inj.
      * destruct HFF1. apply IHn with (F := fun f => skip_fun x F f).
        intros ? ?. now apply skip_fun_inj.
Qed.

Lemma fin_sum_dec :
  forall (n : nat) (P : Fin.t n -> Prop),
    (forall f : Fin.t n, P f \/ ~ P f) ->
    (exists f : Fin.t n, P f) \/ ~ (exists f : Fin.t n, P f).
Proof.
  induction n; intros ? HPdec.
  - right. intro Hexists. destruct Hexists. now apply of_Fin_0.
  - specialize HPdec with Fin.F1 as HP1dec. destruct HP1dec as [PF1 | notPF1].
    + left. now exists Fin.F1.
    + destruct IHn with (fun f => P (Fin.FS f)) as [Hex | Hnex].
      * trivial.
      * destruct Hex. left. now exists (Fin.FS x).
      * right. intro Hex. destruct Hex. dependent destruction x.
        -- now apply notPF1.
        -- apply Hnex. now exists x.
Qed.

Lemma fin_sum_dneg :
  forall (n : nat) (P : Fin.t n -> Prop),
    (forall f : Fin.t n, P f \/ ~ P f) ->
    ~~ (exists f : Fin.t n, P f) ->
    exists f : Fin.t n, P f.
Proof.
  intros ? ? HPdec HdnegSum.
  now destruct fin_sum_dec with (P := P) as [Hsum | Hnsum].
Qed.

Fact fin_not_sum_prod_not :
  forall (n : nat) (P : Fin.t n -> Prop),
    (~ exists f : Fin.t n, P f) ->
    forall f : Fin.t n, ~ P f.
Proof.
  eauto.
Qed.

Fact fin_eq_dec :
  forall (n : nat) (f f' : Fin.t n),
    {f = f'} + {f <> f'}.
Proof.
  induction f; dependent destruction f'.
  - now left.
  - now right.
  - now right.
  - destruct IHf with f' as [Heq | Hneq].
    + left. now f_equal.
    + right. intro Heq. apply Hneq. now apply Fin.FS_inj.
Qed.

Lemma fin_n_inj_sur :
  forall (n : nat) (F : Fin.t n -> Fin.t n) (f : Fin.t n),
    (forall f f' : Fin.t n, F f = F f' -> f = f') ->
    exists f' : Fin.t n, F f' = f.
Proof.
  intros ? ? ? Hinj. apply fin_sum_dneg.
  - intro f'. destruct fin_eq_dec with n (F f') f.
    + now left.
    + now right.
  - intro Hnsum. pose proof (Hallnot := fin_not_sum_prod_not _ _ Hnsum).
    simpl in Hallnot. destruct n.
    + now apply of_Fin_0.
    + destruct n.
      * apply Hallnot with Fin.F1. rewrite all_fin_1. apply all_fin_1.
      * apply PeanoNat.Nat.nle_succ_diag_l with (S n).
        apply drawer_principle with (fun f' => skip_always f (F f')).
        intros f' f''. unfold skip_always.
        pose proof (HskipInj := skip_inj _ f (F f') (F f'')).
        destruct (skip _ (F f')) eqn:Hskip', (skip _ (F f'')) eqn:Hskip'';
        intro Heq; apply Hinj; apply HskipInj.
        -- now f_equal.
        -- destruct Hallnot with f''. symmetry. now apply skip_none.
        -- destruct Hallnot with f'. symmetry. now apply skip_none.
        -- easy.
Qed.

Lemma fin_n_inj_lt_n :
  forall (n k : nat) (F : Fin.t n -> nat),
    (forall f : Fin.t n, F f < n) ->
    (forall f f' : Fin.t n, F f = F f' -> f = f') ->
    k < n ->
    exists f : Fin.t n, F f = k.
Proof.
  intros ? ? ? HUpBound Hinj Hin. destruct n.
  - now apply PeanoNat.Nat.nlt_0_r in Hin.
  - assert (exists f : Fin.t (S n), fin_ofNat n (F f) = fin_ofNat n k) as Hex.
    + apply fin_n_inj_sur. intros ? ? Heq. apply Hinj.
      apply fin_ofNat_inj with n; intuition.
    + destruct Hex as [f Heq]. exists f. apply fin_ofNat_inj with n; intuition.
Qed.

Lemma fin_n_not_inj :
  forall (n : nat) (F : Fin.t n -> nat),
    (~ forall f f' : Fin.t n, F f = F f' -> f = f') ->
    exists f f' : Fin.t n, f <> f' /\ F f = F f'.
Proof.
  intros ? ? Hninj. apply fin_sum_dneg.
  - intro. apply fin_sum_dec. intro f'.
    destruct fin_eq_dec with n f f' as [Heq | Hneq].
    + now right.
    + destruct PeanoNat.Nat.eq_dec with (F f) (F f') as [HFeq | HFneq].
      * now left.
      * now right.
  - intro Hnsum. apply Hninj. intros ? ? HFeq.
    destruct fin_eq_dec with n f f' as [Heq | Hneq].
    + assumption.
    + destruct Hnsum. eauto.
Qed.

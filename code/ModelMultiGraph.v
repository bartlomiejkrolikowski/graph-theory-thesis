Require Fin.
Require Graphs.Aux.Fin.

(* finite graph represented by a matrix *)
Definition t (n : nat) : Type := Fin.t n -> Fin.t n -> nat.

(* number of edges between two vertices *)
Definition edge {n : nat} (g : t n) : Fin.t n -> Fin.t n -> nat := g.

(* graph does not contain loops *)
Definition isIrreflexive (n : nat) (g : t n) : Prop :=
  forall v : Fin.t n, g v v = 0.

(* graph is undirected *)
Definition isSymmetric (n : nat) (g : t n) : Prop :=
  forall u v : Fin.t n, g u v = g v u.

(* there are no multiple edges *)
Definition noMultiEdge (n : nat) (g : t n) : Prop :=
  forall u v : Fin.t n, g u v <= 1.

(* it is a simple graph *)
Definition isSimple (n : nat) (g : t n) : Prop :=
  isIrreflexive n g /\ isSymmetric n g /\ noMultiEdge n g.

(* a subgraph with one less vertex *)
Definition ofSucc {n : nat} (g : t (S n)) : t n :=
  fun u v : Fin.t n => g (Fin.FS u) (Fin.FS v).

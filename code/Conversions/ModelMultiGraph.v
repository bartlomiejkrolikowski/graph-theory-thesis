Require Graphs.ModelMultiGraph.
Require Graphs.MultiGraph.

Definition toModel {n : nat} (g : MultiGraph.t n) : ModelMultiGraph.t n :=
  MultiGraph.edge n g.

Fixpoint ofModel {n : nat} (g : ModelMultiGraph.t n) : MultiGraph.t n :=
  match n return ModelMultiGraph.t n -> MultiGraph.t n with
  | 0 => fun _ => MultiGraph.empty
  | S n' =>
    fun g : ModelMultiGraph.t (S n') =>
      MultiGraph.cons n' (fun v : Fin.t n' => g Fin.F1 (Fin.FS v))
        (ofModel (ModelMultiGraph.ofSucc g))
  end g.

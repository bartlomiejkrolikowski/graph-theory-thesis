Require Import Coq.Program.Equality.
Require Import FunctionalExtensionality.

Require Graphs.Proofs.MultiGraph.
Require Graphs.Proofs.ModelMultiGraph.

Require Import Graphs.Conversions.ModelMultiGraph.

Lemma isSimple_ofModel :
  forall (n : nat) (g : ModelMultiGraph.t n),
    ModelMultiGraph.isSimple n g ->
    MultiGraph.isSimple (ofModel g).
Proof.
  induction n; simpl; intros ? Hsimp.
  - exact MultiGraph.simpEmpty.
  - apply MultiGraph.simpCons.
    + destruct Hsimp as [HIrr [HSym HNoMul]]. intro. apply HNoMul.
    + apply IHn. now apply ModelMultiGraph.isSimple_ofSucc.
Qed.

Lemma isSimple_toModel :
  forall (n : nat) (g : MultiGraph.t n),
    MultiGraph.isSimple g ->
    ModelMultiGraph.isSimple n (toModel g).
Proof.
  intros n g Hsimp. unfold toModel, ModelMultiGraph.isSimple. repeat split.
  - unfold ModelMultiGraph.isIrreflexive. apply MultiGraph.edge_diag.
  - unfold ModelMultiGraph.isSymmetric. apply MultiGraph.edge_symm.
  - unfold ModelMultiGraph.noMultiEdge. intros.
    now apply MultiGraph.edge_if_simple.
Qed.

(* toModel and ofModel are bijections *)

Lemma isSimple_toOfModel :
  forall (n : nat) (g : MultiGraph.t n),
    MultiGraph.isSimple g ->
    ofModel (toModel g) = g.
Proof.
  intros ? ? Hsimp. induction Hsimp; cbn.
  - reflexivity.
  - now f_equal.
Qed.

Lemma isSimple_ofToModel :
  forall (n : nat) (g : ModelMultiGraph.t n),
    ModelMultiGraph.isSimple n g ->
    toModel (ofModel g) = g.
Proof.
  intros ? ? [HIrr [Hsym HNoMul]]. extensionality u. extensionality v.
  induction u; cbn; dependent destruction v.
  - easy.
  - easy.
  - easy.
  - rewrite IHu.
    + easy.
    + now apply ModelMultiGraph.isIrreflexive_ofSucc.
    + now apply ModelMultiGraph.isSymmetric_ofSucc.
    + now apply ModelMultiGraph.noMultiEdge_ofSucc.
Qed.

(* toModel and ofModel are graph isomorphisms *)

Fact isSimple_iso_ofModel :
  forall (n : nat) (g : ModelMultiGraph.t n),
    ModelMultiGraph.isSimple n g ->
    MultiGraph.edge n (ofModel g) = ModelMultiGraph.edge g.
Proof.
  apply isSimple_ofToModel.
Qed.

Fact iso_toModel :
  forall (n : nat) (g : MultiGraph.t n),
    ModelMultiGraph.edge (toModel g) = MultiGraph.edge n g.
Proof.
  easy.
Qed.

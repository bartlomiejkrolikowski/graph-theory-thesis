Require Import Coq.Program.Equality.
Require Import FunctionalExtensionality.

Require Fin.
Require List.
Import List.ListNotations.
Require Nat.

Require Graphs.Aux.Fin.

Inductive t : nat -> Type :=
| empty : t 0
| cons : forall (n : nat) (edges : Fin.t n -> nat), t n -> t (S n).

Inductive isSimple : forall {n : nat}, t n -> Prop :=
| simpEmpty : isSimple MultiGraph.empty
| simpCons : forall {n : nat} (e : Fin.t n -> nat) (g : MultiGraph.t n),
               (forall u : Fin.t n, e u <= 1) ->
               isSimple g -> isSimple (cons n e g).

(* a graph with one vertex *)
Definition single : t 1 :=
  cons 0 Fin.of_Fin_0 empty.

(* a graph with no edges *)
Fixpoint no_edge (n : nat) : t n :=
  match n as k return t k with
  | 0 => empty
  | S n' => cons n' (fun _ => 0) (no_edge n')
  end.

(* full simple graph *)
Fixpoint full (n : nat) : t n :=
  match n as k return t k with
  | 0 => empty
  | S n' => cons n' (fun _ => 1) (full n')
  end.

(* number of edges between two vertices *)
Fixpoint edge (n : nat) (g : t n) : Fin.t n -> Fin.t n -> nat :=
  match g in t k return Fin.t k -> Fin.t k -> nat with
  | empty => Fin.of_Fin_0
  | cons n' e g' =>
    fun u v : Fin.t (S n') =>
      match u in Fin.t k
        return Fin.t k ->
               (Fin.t (Nat.pred k) -> Fin.t (Nat.pred k) -> nat) ->
               (Fin.t (Nat.pred k) -> nat) -> nat
      with
      | @Fin.F1 n'' =>
        fun v : Fin.t (S n'') =>
          match v in Fin.t k
            return (Fin.t (Nat.pred k) -> Fin.t (Nat.pred k) -> nat) ->
                   (Fin.t (Nat.pred k) -> nat) -> nat
          with
          | @Fin.F1 n''' => fun _ _ => 0
          | @Fin.FS n''' v' =>
            fun _ (F : Fin.t n''' -> nat) => F v'
          end
      | @Fin.FS n'' u' =>
        fun v : Fin.t (S n'') =>
          match v in Fin.t k
            return Fin.t (Nat.pred k) ->
                   (Fin.t (Nat.pred k) -> Fin.t (Nat.pred k) -> nat) ->
                   (Fin.t (Nat.pred k) -> nat) -> nat
          with
          | @Fin.F1 n''' =>
            fun (u : Fin.t n''') _ (F : Fin.t n''' -> nat) => F u
          | @Fin.FS n''' v' =>
            fun (u : Fin.t n''') (E : Fin.t n''' -> Fin.t n''' -> nat) _ =>
              E u v'
          end u'
      end v (edge n' g') e
  end.

(* add an edge to the graph *)
Fixpoint add_edge (n : nat) (g : t n) : Fin.t n -> Fin.t n -> t n :=
  match g in t k return Fin.t k -> Fin.t k -> t k with
  | empty => fun _ _ => empty
  | cons n' e g' =>
    fun u v : Fin.t (S n') =>
      match u in Fin.t k
        return Fin.t k ->
               (Fin.t (Nat.pred k) -> Fin.t (Nat.pred k) -> t (Nat.pred k)) ->
               (Fin.t (Nat.pred k) -> nat) -> t (Nat.pred k) -> t k
      with
      | @Fin.F1 n'' =>
        fun v : Fin.t (S n'') =>
          match v in Fin.t k
            return (Fin.t (Nat.pred k) ->
                      Fin.t (Nat.pred k) -> t (Nat.pred k)) ->
                   (Fin.t (Nat.pred k) -> nat) -> t (Nat.pred k) -> t k
          with
          | @Fin.F1 n''' =>
            fun _ (e : Fin.t n''' -> nat) (g' : t n''') => cons n''' e g'
          | @Fin.FS n''' v' =>
            fun _ (e : Fin.t n''' -> nat) (g : t n''') =>
              cons n''' (Fin.S_point v' e) g
          end
      | @Fin.FS n'' u' =>
        fun v : Fin.t (S n'') =>
          match v in Fin.t k
            return Fin.t (Nat.pred k) ->
                   (Fin.t (Nat.pred k) ->
                      Fin.t (Nat.pred k) -> t (Nat.pred k)) ->
                   (Fin.t (Nat.pred k) -> nat) -> t (Nat.pred k) -> t k
          with
          | @Fin.F1 n''' =>
            fun (u' : Fin.t n''') _ (e : Fin.t n''' -> nat) (g : t n''') =>
              cons n''' (Fin.S_point u' e) g
          | @Fin.FS n''' v' =>
            fun (u' : Fin.t n''') (E : Fin.t n''' -> Fin.t n''' -> t n''')
                (e : Fin.t n''' -> nat) _ =>
              cons n''' e (E u' v')
          end u'
      end v (add_edge n' g') e g'
  end.

(* number of edges going into the vertex (from higher vertices) *)
Fixpoint deg_forward (n : nat) (g : t n) (f : Fin.t n) : nat :=
  match f in Fin.t k return t k -> nat with
  | Fin.F1 => fun _ => 0
  | @Fin.FS n' f' =>
    fun g : t (S n') =>
      match g in t k return Fin.t (Nat.pred k) -> nat with
      | empty => Fin.of_Fin_0
      | cons n' e g' =>
        fun f : Fin.t n' => e f + deg_forward n' g' f
      end f'
  end g.

(* number of edges going out of the vertex (to lower vertices) *)
Fixpoint deg_backward (n : nat) (g : t n) : Fin.t n -> nat :=
  match g in t k return Fin.t k -> nat with
  | empty => Fin.of_Fin_0
  | cons n' e g' =>
    fun f : Fin.t (S n') =>
      match f in Fin.t k
        return (Fin.t (Nat.pred k) -> nat) ->
               (Fin.t (Nat.pred k) -> nat) -> nat
      with
      | @Fin.F1 n'' =>
        fun _ (F : Fin.t n'' -> nat) => Fin.sum F
      | @Fin.FS n'' f' => fun (F : Fin.t n'' -> nat) _ => F f'
      end (deg_backward n' g') e
  end.

Definition deg (n : nat) (g : t n) (f : Fin.t n) : nat :=
  deg_forward n g f + deg_backward n g f.

(* total count of edges in the graph *)
Fixpoint sum_edges (n : nat) (g : t n) : nat :=
  match g with
  | empty => 0
  | cons n' e g' =>
      List.list_sum (List.map e (Fin.seq n')) + sum_edges n' g'
  end.
